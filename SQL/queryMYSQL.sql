ALTER TABLE YV_USERSEVENTS DROP FOREIGN KEY usersEventsUidFk;
ALTER TABLE YV_USERSEVENTS DROP FOREIGN KEY usersEventsEidFk;
DROP TABLE  YV_USERSEVENTS;

ALTER TABLE YV_USERS DROP PRIMARY KEY;
ALTER TABLE YV_USERS DROP FOREIGN KEY usersPrivilegeFk;
DROP TABLE YV_USERS ;

ALTER TABLE YV_ACCESS DROP PRIMARY KEY;
DROP TABLE YV_ACCESS;

ALTER TABLE YV_EVENTS DROP PRIMARY KEY;
ALTER TABLE YV_EVENTS DROP FOREIGN KEY eventsThemeFk;
DROP TABLE  YV_EVENTS;

ALTER TABLE YV_THEMES DROP PRIMARY KEY;
DROP TABLE  YV_THEMES;

DROP TABLE  YV_MESSAGES;


CREATE TABLE YV_ACCESS(
  privilege VARCHAR(100) NOT NULL,
  createDeleteUser INTEGER NOT NULL,
  createDeleteTheme INTEGER NOT NULL,
  deleteOthersEvents INTEGER NOT NULL,
  createDeleteEvent INTEGER NOT NULL,
  subUnsubEvent INTEGER NOT NULL,
  rateEvent INTEGER NOT NULL,
  commentEvent INTEGER NOT NULL,
  useSearch INTEGER NOT NULL,
  CONSTRAINT accessPrivilegePk PRIMARY KEY(privilege)
);


CREATE TABLE YV_USERS(id INTEGER,
name VARCHAR(100),
pwd VARCHAR(100),
privilege VARCHAR(100) NOT NULL,
age INTEGER,
mail VARCHAR(100),
CONSTRAINT usersIdPk PRIMARY KEY(id),
CONSTRAINT usersPrivilegeFk FOREIGN KEY(privilege) references YV_ACCESS(privilege));


CREATE TABLE YV_THEMES(
  theme VARCHAR(100),
  CONSTRAINT themePk PRIMARY KEY(theme)
);

CREATE TABLE YV_EVENTS(
  id INTEGER,
  theme VARCHAR(100) NOT NULL,
  name VARCHAR(100) NOT NULL,
  day DATE NOT NULL,
  description VARCHAR(100) NOT NULL,
  maxEffective INTEGER,
  minEffective INTEGER,
  effective INTEGER,
  address VARCHAR(100) NOT NULL,
  latitude NUMERIC(8, 8),
  longitude NUMERIC(8, 8),
  pending INTEGER NOT NULL,
  CONSTRAINT eventsIdPk PRIMARY KEY(id),
  CONSTRAINT eventsThemeFk FOREIGN KEY(theme) references YV_THEMES(theme)
);

CREATE TABLE YV_USERSEVENTS(
  userId INTEGER,
  eventId INTEGER,
  pending INTEGER,
  CONSTRAINT usersEventsUidFk FOREIGN KEY(userId) references YV_USERS(id) ON DELETE CASCADE,
  CONSTRAINT usersEventsEidFk FOREIGN KEY(eventId) references YV_EVENTS(id) ON DELETE CASCADE
);


CREATE TABLE YV_MESSAGES(
  titre VARCHAR(100) NOT NULL,	
  message VARCHAR(500) NOT NULL,
  uIdD INTEGER,
  uIdR INTEGER
);
/*
insert into yv_access values('ADMIN',1,1,1,1,1,1,1,1);
insert into yv_access values('NORMAL',0,0,0,1,1,1,1,1);
*/
/* ~~~~~ idée de trigger n°1~~~~~
 si un événement existe à une distance de 1km la même date
  on envoie un message aux admins et on met le champ de l'évent inséré 
  pending à true et on affiche(pour les non admins) sur le site seulement ceux qui ont pending à false.
  Les admins peuvent ensuite vérifier sur le site si les deux évents sont différents et passer pending à false.
 
*/
 
DELIMITER //

CREATE OR REPLACE TRIGGER EVENTEXISTS BEFORE INSERT ON YV_EVENTS
       FOR EACH ROW	  
BEGIN
	DECLARE	eventExists INTEGER;
	DECLARE finished INTEGER DEFAULT 0;
	DECLARE ad INTEGER;
	DECLARE adCursor cursor for (SELECT id FROM YV_USERS WHERE privilege='admin');
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;
	-- sélection des évènements proches et le même jour que celui inséré
	SELECT count(*) into eventExists FROM YV_EVENTS WHERE
	ABS(latitude-new.longitude)<=100 AND
	ABS(longitude-new.latitude)<=100 AND
	day=new.day;
	-- si on a trouvé un tel évènement on demande aux administrateurs de vérifier
	-- en leur envoyant un message.
	IF(eventExists>0) THEN
	        UPDATE YV_EVENTS SET pending = 1  WHERE id = new.id;
		OPEN adCursor;
		admin_loop : LOOP
		   FETCH adCursor INTO ad;
		   IF finished = 1 THEN
		      LEAVE admin_loop;
		   END IF;
		   INSERT INTO YV_MESSAGES VALUES
		       ('Evenement en attente de vérification',
		       CONCAT('Vérifier l évènement n°', new.id),0,ad.id);
		end LOOP admin_loop;
	END IF;
END //

/* ~~~~~ idée de trigger n°2~~~~~
 Si un utilisateur s'inscrit à deux évenement à la même date on passe les deux pending à true 
 et on envoie un message à l'utilisateur lui demandant de choisir l'un des deux ce qui supprimera l'un des deux
 et passera pending à false.
 */
 


CREATE OR REPLACE TRIGGER SUBSAMEDAY BEFORE INSERT ON YV_USERSEVENTS
       FOR EACH ROW	  
BEGIN
       DECLARE subSameDay INTEGER;
       DECLARE newEventDay DATE;
	-- sélection du jour du nouvel évènement
	SELECT day into newEventDay FROM YV_EVENTS WHERE id=new.eventId;
	-- sélection des évènements inscrits le même jour que celui inséré
	SELECT count(*) into subSameDay FROM YV_USERSEVENTS
	JOIN YV_EVENTS E ON E.id=eventId
	WHERE userId=new.userId AND E.day=newEventDay ;
	-- si on a trouvé un tel évènement on demande à l'utilisateur de choisir
	-- en lui envoyant un message.
	IF(subSameDay>0) THEN
	        UPDATE YV_USERSEVENTS SET pending = 1  WHERE userId = new.userId;
		SET new.pending = 1;
		INSERT INTO YV_MESSAGES VALUES
		    ('Evenement en attente de vérification',
		    'Vous êtes inscrit à deux évènements la même dâte.',0,new.userId);
	END IF;
END //	
DELIMITER ;
